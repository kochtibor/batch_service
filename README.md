##Batch update

This small service can be used to update missing data in a dataset.

The dataset is accepted in CSV format.

The service is available at http://52.27.137.233:8080/ with the following endpoints:

- /api/jobs
    - POST: files can be sent with multipart/form-data requests, a 'file' property has to be added to the form data  
            the API key for the Whitepages API PRO has to be sent in the 'API-KEY' header field  
            in the response the Location header contains the url to be used for polling the status of the job
    - GET: if a request has finished successfully, the result file can be accessed with this request

- /api/status
    - GET: can be used to query the status of a previously submitted job  
           when the job is finished the response contains the resource url
  
  

  
Example process:  
- a request is POSTed to /api/jobs and the response Location header is '<server_name>/api/status/2'  
- while the job is in progress the status can be queried at the above url  
- when it's finished the response will have the resource url, e.g. '<server_name>/api/jobs/2, the result csv file can be downloaded from here

Used technologies:  
- Spring Boot  
- Spring Data  
- Guava cache
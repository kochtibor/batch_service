import static ch.qos.logback.classic.Level.INFO
import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.status.OnConsoleStatusListener
import ch.qos.logback.core.rolling.RollingFileAppender
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy

statusListener(OnConsoleStatusListener)

appender("STDOUT", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%d %p [%c] - %m%n"
    }
}

appender("FILE", RollingFileAppender) {
    file = "logs/batchService.log"
    rollingPolicy(TimeBasedRollingPolicy) {
        fileNamePattern = "logs/batchService.%d{yyyy-MM-dd}.log.gz"
        maxHistory = 30
    }
    encoder(PatternLayoutEncoder) {
        pattern = "%d %p [%c-%M] - %m%n"
    }
}


root(INFO, ["STDOUT", "FILE"])
logger("batchservice", DEBUG)
package batchservice.rest;

import batchservice.Constants;
import batchservice.domain.Job;
import batchservice.domain.JobStatus;
import batchservice.domain.StatusResponse;
import batchservice.exception.BatchException;
import batchservice.service.JobService;
import com.google.common.io.ByteStreams;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RestController
public class BatchRestController {

    private static final String JOBS_ENDPOINT = "/api/jobs";
    private static final String STATUS_ENDPOINT = "/api/status";
    private static final List<String> ALLOWED_CONTENT_TYPES = new ArrayList<>(Arrays.asList("text/csv", "application/csv", "application/octet-stream", "application/vnd.ms-excel"));

    @Autowired
    private JobService jobService;

    @Value("${work.dir}")
    private String workDir;

    @RequestMapping(value = JOBS_ENDPOINT, method = RequestMethod.POST)
    public void uploadCSV(@RequestParam("file") MultipartFile file, @RequestHeader("API-KEY") String apiKey, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (file.isEmpty()) {
            throw new BatchException("The file you tried to upload is empty!");
        }

        if (!ALLOWED_CONTENT_TYPES.contains(file.getContentType())) {
            throw new BatchException("Unsupported content type!");
        }

        if (StringUtils.isEmpty(apiKey)) {
            throw new BatchException("There wasn't an api key provided!");
        }

        final String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        File jobDir = new File(workDir, "batch" + uuid);
        jobDir.mkdir();

        File csvFile = new File(jobDir, "request.csv");
        file.transferTo(csvFile);

        Job job = jobService.submitJob(csvFile, apiKey);

        response.setStatus(HttpServletResponse.SC_ACCEPTED);
        response.setHeader("Location", getBaseUri(request) + STATUS_ENDPOINT + "/" + job.getId());
    }

    @RequestMapping(value = STATUS_ENDPOINT + "/{id}", method = RequestMethod.GET)
    public StatusResponse getStatus(@PathVariable("id") String jobId, HttpServletRequest request, HttpServletResponse response) throws IOException {
        StatusResponse statusResponse = new StatusResponse();
        Job job = jobService.getJob(Long.parseLong(jobId));

        if (job != null) {
            JobStatus status = job.getStatus();
            statusResponse.setStatus(status);
            statusResponse.setMessages(job.getMessages());
            if (status == JobStatus.FINISHED) {
                statusResponse.setResource(getBaseUri(request) + JOBS_ENDPOINT + "/" + job.getId());
            }
        } else {
            throw new BatchException("Invalid job id!");
        }

        return statusResponse;
    }

    @RequestMapping(value = JOBS_ENDPOINT + "/{id}", method = RequestMethod.GET)
    public void getResult(@PathVariable("id") String jobId, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Job job = jobService.getJob(Long.parseLong(jobId));

        if (job != null) {
            JobStatus status = job.getStatus();
            if (status == JobStatus.FINISHED) {
                File resultFile = new File(job.getJobDirectory(), Constants.RESULT_CSV);
                InputStream is = new FileInputStream(resultFile);

                response.setContentType("text/csv");
                response.setHeader("Content-Disposition", "attachment; filename=result.csv");
                response.setContentLength((int) resultFile.length());

                ServletOutputStream out = response.getOutputStream();
                ByteStreams.copy(is, out);
                out.flush();
                out.close();
                response.flushBuffer();
            } else {
                throw new BatchException("Job hasn't finished yet or failed!");
            }
        } else {
            throw new BatchException("Invalid job id!");
        }
    }

    private String getBaseUri(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort();
    }
}

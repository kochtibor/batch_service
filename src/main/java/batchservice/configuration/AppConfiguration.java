package batchservice.configuration;

import batchservice.domain.Contact;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Configuration
public class AppConfiguration {

    @Bean(name = "jobExecutor")
    public ExecutorService getJobExecutor() {
        return Executors.newFixedThreadPool(4);
    }

    @Bean(name = "personCache")
    public Cache getPersonCache() {
        Cache<String, Contact> personCache = CacheBuilder.newBuilder()
                .expireAfterAccess(5, TimeUnit.MINUTES)
                .expireAfterWrite(6, TimeUnit.MINUTES)
                .maximumSize(10000)
                .build();
        return personCache;
    }

    @Bean(name = "locationCache")
    public Cache getLocationCache() {
        Cache locationCache = CacheBuilder.newBuilder()
                .expireAfterAccess(5, TimeUnit.MINUTES)
                .expireAfterWrite(6, TimeUnit.MINUTES)
                .maximumSize(10000)
                .build();
        return locationCache;
    }

    @Bean(name = "phoneCache")
    public Cache getPhoneCache() {
        Cache phoneCache = CacheBuilder.newBuilder()
                .expireAfterAccess(5, TimeUnit.MINUTES)
                .expireAfterWrite(6, TimeUnit.MINUTES)
                .maximumSize(10000)
                .build();
        return phoneCache;
    }

}

package batchservice.service;

public class JobSettings {
    private int namePosition = -1;
    private int phonePosition = -1;
    private int addressPosition = -1;
    private String apiKey;

    public int getNamePosition() {
        return namePosition;
    }

    public void setNamePosition(int namePosition) {
        this.namePosition = namePosition;
    }

    public int getPhonePosition() {
        return phonePosition;
    }

    public void setPhonePosition(int phonePosition) {
        this.phonePosition = phonePosition;
    }

    public int getAddressPosition() {
        return addressPosition;
    }

    public void setAddressPosition(int addressPosition) {
        this.addressPosition = addressPosition;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}

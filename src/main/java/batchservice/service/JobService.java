package batchservice.service;

import batchservice.domain.Job;
import batchservice.domain.JobStatus;
import batchservice.repository.JobRepository;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;

@Service
public class JobService {
    private static final Logger LOGGER = LoggerFactory.getLogger(JobService.class);

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    @Qualifier("jobExecutor")
    private ExecutorService jobExecutor;

    @Autowired
    private BatchService batchService;

    public Job submitJob(File csvFile, String apiKey) throws IOException {
        Job job = new Job();
        FilenameUtils.getFullPath(csvFile.getCanonicalPath());
        job.setJobDirectory(FilenameUtils.getFullPath(csvFile.getCanonicalPath()));
        jobRepository.save(job);

        jobExecutor.submit(() -> {
            try {
                updateJobStatus(job, JobStatus.IN_PROGRESS);
                batchService.processCSV(csvFile, apiKey);
                updateJobStatus(job, JobStatus.FINISHED);
            } catch(Exception e) {
                LOGGER.error("An unexpected error occurred during the update!", e);
                job.getMessages().add(e.getMessage());
                updateJobStatus(job, JobStatus.FAILED);
            }
        });

        return job;
    }

    public Job getJob(Long jobId) {
        Job job = jobRepository.findOne(jobId);
        return job;
    }

    private void updateJobStatus(Job job, JobStatus status) {
        job.setStatus(status);
        jobRepository.save(job);
    }

    @Scheduled(cron="0 0 2 * * ?")
    public void removeFailedJobs() {
        Date date = DateUtils.addDays(new Date(), -10);
        List<Job> jobList = jobRepository.findByCutoffDateAndStatus(date, JobStatus.FAILED);
        for (Job job : jobList) {
            File jobDir = new File(job.getJobDirectory());
            try {
                FileUtils.deleteDirectory(jobDir);
            } catch (IOException ioe) {
                LOGGER.warn("Couldn't delete job directory: " + jobDir, ioe);
            }
        }

        jobRepository.delete(jobList);
    }
}

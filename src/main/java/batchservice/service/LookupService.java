package batchservice.service;

import batchservice.domain.Address;
import com.whitepages.proapi.api.client.Client;
import com.whitepages.proapi.api.client.FindException;
import com.whitepages.proapi.api.query.BusinessQuery;
import com.whitepages.proapi.api.query.LocationQuery;
import com.whitepages.proapi.api.query.PersonQuery;
import com.whitepages.proapi.api.query.PhoneQuery;
import com.whitepages.proapi.api.response.Response;
import com.whitepages.proapi.data.entity.Business;
import com.whitepages.proapi.data.entity.Location;
import com.whitepages.proapi.data.entity.Person;
import com.whitepages.proapi.data.entity.Phone;
import com.whitepages.proapi.data.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LookupService {
    private static final Logger LOGGER = LoggerFactory.getLogger(LookupService.class);

    public List<Person> findByPerson(Client client, PersonQuery query) throws FindException {
        Response<Person> response = client.findPeople(query);

        List<Person> results = null;
        if (response.isSuccess()) {
            results = response.getResults();
        } else {
            for (Message message : response.getResponseMessages().getMessageList()) {
                LOGGER.warn("FindPerson lookup error message: {}", message.getMessage());
            }
        }

        return results;
    }

    public List<Business> findByBusiness(Client client, BusinessQuery query) throws FindException {
        Response<Business> response = client.findBusinesses(query);

        List<Business> results = null;
        if (response.isSuccess()) {
            results = response.getResults();
        } else {
            for (Message message : response.getResponseMessages().getMessageList()) {
                LOGGER.warn("FindBusiness lookup error message: {}", message.getMessage());
            }
        }

        return results;
    }

    public List<Location> findByLocation(Client client, LocationQuery query) throws FindException {
        Response<Location> response = client.findLocations(query);

        List<Location> results = null;
        if (response.isSuccess()) {
            results = response.getResults();
        } else {
            for (Message message : response.getResponseMessages().getMessageList()) {
                LOGGER.warn("ReverseAddress lookup error message: {}", message.getMessage());
            }
        }

        return results;
    }

    public List<Phone> findByPhone(Client client, PhoneQuery query) throws FindException {
        Response<Phone> response = client.findPhones(query);

        List<Phone> results = null;
        if (response.isSuccess()) {
            results = response.getResults();
        } else {
            for (Message message : response.getResponseMessages().getMessageList()) {
                LOGGER.warn("ReversePhone lookup error message: {}", message.getMessage());
            }
        }

        return results;
    }

    public boolean isLocationMatch(Location location, Address address) {
        boolean isMatch = true;
        if (address.getCity() != null) {
            isMatch = location.getCity() != null && location.getCity().equalsIgnoreCase(address.getCity());
        }
        if (address.getState() != null && isMatch) {
            isMatch = location.getStateCode() != null && location.getStateCode().equalsIgnoreCase(address.getState());
        }
        if (address.getZip() != null && isMatch) {
            isMatch = location.getPostalCode() != null && location.getPostalCode().equalsIgnoreCase(address.getZip());
        }
        if (address.getStreet() != null && isMatch) {
            isMatch = location.getAddress() != null && location.getAddress().toLowerCase().contains(address.getStreet().toLowerCase());
        }

        return isMatch;
    }
}

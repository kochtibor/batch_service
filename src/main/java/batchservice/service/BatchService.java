package batchservice.service;

import batchservice.Constants;
import batchservice.csv.CSVFormat;
import batchservice.csv.CSVParser;
import batchservice.csv.CSVPrinter;
import batchservice.csv.CSVRecord;
import batchservice.domain.Address;
import batchservice.domain.Contact;
import batchservice.exception.UpdateException;
import batchservice.utils.AddressParserUtils;
import batchservice.utils.CommonUtils;
import com.google.common.base.Joiner;
import com.google.common.cache.Cache;
import com.whitepages.proapi.api.client.Client;
import com.whitepages.proapi.api.client.FindException;
import com.whitepages.proapi.api.query.BusinessQuery;
import com.whitepages.proapi.api.query.LocationQuery;
import com.whitepages.proapi.api.query.PersonQuery;
import com.whitepages.proapi.api.query.PhoneQuery;
import com.whitepages.proapi.data.entity.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
public class BatchService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchService.class);

    private static final int BATCH_SIZE = 1000;

    @Autowired
    private LookupService lookupService;

    @Autowired
    @Qualifier("personCache")
    private Cache<String, Contact> personCache;

    @Autowired
    @Qualifier("locationCache")
    private Cache<String, Contact> locationCache;

    @Autowired
    @Qualifier("phoneCache")
    private Cache<String, Contact> phoneCache;


    public File processCSV(File csvFile, String apiKey) throws IOException {
        CSVParser parser = new CSVParser(new FileReader(csvFile), CSVFormat.DEFAULT.withHeader());
        Map<String, Integer> headerMap = parser.getHeaderMap();

        JobSettings settings = CommonUtils.getColumnPositions(headerMap);
        if (settings.getAddressPosition() == -1
                || settings.getNamePosition() == -1
                || settings.getPhonePosition() == -1) {
            throw new UpdateException("Couldn't parse the file header!");
        }
        settings.setApiKey(apiKey);

        List<Future> futures = new ArrayList<>();
        List<CSVRecord> records = new ArrayList<>();
        List<CSVRecord> allRecords = new ArrayList<>();

        ExecutorService lookupExecutor = Executors.newFixedThreadPool(10);
        for (CSVRecord record : parser) {
            records.add(record);
            if (parser.getRecordNumber() % BATCH_SIZE == 0) {
                allRecords.addAll(records);
                List<CSVRecord> copyList = new ArrayList<>(records);

                Future<?> future = lookupExecutor.submit(() -> {
                    updateData(settings, copyList);
                });

                records.clear();
                futures.add(future);
            }
        }

        if (parser.getRecordNumber() % BATCH_SIZE != 0) {
            allRecords.addAll(records);

            Future<?> future = lookupExecutor.submit(() -> {
                updateData(settings, records);
            });

            futures.add(future);
        }

        parser.close();

        for(Future future : futures) {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.error("Couldn't finish all update processes!", e);
                throw new UpdateException("One or more update process failed!");
            }
        }

        return writeFile(allRecords, headerMap, csvFile);
    }

    private File writeFile(List<CSVRecord> records, Map<String, Integer> headerMap, File csvFile) throws IOException {
        FileWriter fileWriter = null;
        CSVPrinter csvPrinter = null;
        File resultFile = new File(FilenameUtils.getFullPath(csvFile.getCanonicalPath()), Constants.RESULT_CSV);

        try {
            fileWriter = new FileWriter(resultFile);

            String header = Joiner.on(",").join(headerMap.keySet());
            fileWriter.append(header).append("\r\n");

            CSVFormat format = CSVFormat.DEFAULT.withNullString("").withSkipHeaderRecord(true);
            csvPrinter = new CSVPrinter(fileWriter, format);

            for (CSVRecord record : records) {
                csvPrinter.printRecord(record.toList());
            }
        } finally {
            if (fileWriter != null) {
                fileWriter.flush();
                fileWriter.close();
            }
            if (csvPrinter != null) {
                csvPrinter.close();
            }
        }

        return resultFile;
    }

    public void updateData(JobSettings settings, List<CSVRecord> records) {
        Client client = new Client(settings.getApiKey());

        for (CSVRecord record : records) {
            String name = record.get(settings.getNamePosition());
            String phoneNumber = record.get(settings.getPhonePosition());
            String addressString = record.get(settings.getAddressPosition());

            boolean isNameBlank = StringUtils.isBlank(name);
            boolean isPhoneBlank = StringUtils.isBlank(phoneNumber);
            boolean isAddressBlank = StringUtils.isBlank(addressString);

            if (isNameBlank || isPhoneBlank || isAddressBlank) {
                Address address = new Address();
                if (!isAddressBlank) {
                    address = AddressParserUtils.parseAddress(addressString);
                }

                Contact contact = new Contact(name, phoneNumber, addressString);
                if (!isNameBlank) {
                    LookupParameters parameters = new LookupParameters(address, settings, client, record, contact);
                    nameLookup(parameters);
                } else if (!isPhoneBlank) {
                    LookupParameters parameters = new LookupParameters(address, settings, client, record, contact);
                    phoneLookup(parameters);
                } else if (!isAddressBlank && address.getStreet() != null) {
                    LookupParameters parameters = new LookupParameters(address, settings, client, record, contact);
                    locationLookup(parameters);
                }
            }
        }
    }

    private void nameLookup(LookupParameters parameters) {
        Contact cachedContact = checkCaches(parameters.getContact());
        if (cachedContact != null) {
            parameters.getRecord().set(parameters.getSettings().getAddressPosition(), cachedContact.getAddress());
            parameters.getRecord().set(parameters.getSettings().getPhonePosition(), cachedContact.getPhoneNumber());
        } else {
            PersonQuery personQuery = new PersonQuery();
            personQuery.setName(parameters.getContact().getName());
            personQuery.setCity(parameters.getAddress().getCity());
            personQuery.setPostalCode(parameters.getAddress().getZip());
            personQuery.setStateCode(parameters.getAddress().getState());

            try {
                List<Person> personList = lookupService.findByPerson(parameters.getClient(), personQuery);

                if (personList != null && personList.size() > 0) {
                    LegalEntity legalEntity = personList.get(0);
                    updateRecord(legalEntity, parameters);
                } else {
                    BusinessQuery businessQuery = new BusinessQuery();
                    businessQuery.setName(parameters.getContact().getName());
                    businessQuery.setCity(parameters.getAddress().getCity());
                    businessQuery.setPostalCode(parameters.getAddress().getZip());
                    businessQuery.setStateCode(parameters.getAddress().getState());

                    List<Business> businessList = lookupService.findByBusiness(parameters.getClient(), businessQuery);
                    if (businessList != null && businessList.size() > 0) {
                        LegalEntity legalEntity = businessList.get(0);
                        updateRecord(legalEntity, parameters);
                    }
                }
            } catch (FindException fe) {
                LOGGER.error("FindPerson lookup failed!", fe);
            }
            updateCaches(parameters.getContact().getName(),
                    parameters.getRecord().get(parameters.getSettings().getPhonePosition()),
                    parameters.getRecord().get(parameters.getSettings().getAddressPosition()));
        }
    }

    private void updateRecord(LegalEntity legalEntity, LookupParameters parameters) {
        if (StringUtils.isBlank(parameters.getContact().getAddress())) {
            Location location = getLocation(legalEntity);
            if (location != null) {
                parameters.getRecord().set(parameters.getSettings().getAddressPosition(), location.getAddress());
            }
        }

        if (StringUtils.isBlank(parameters.getContact().getPhoneNumber())) {
            List<Phone> phoneNumberList = legalEntity.getPhones();
            if (phoneNumberList != null && phoneNumberList.size() > 0) {
                parameters.getRecord().set(parameters.getSettings().getPhonePosition(), phoneNumberList.get(0).getPhoneNumber());
            }
        }
    }

    private void phoneLookup(LookupParameters parameters) {
        Contact cachedContact = checkCaches(parameters.getContact());
        if (cachedContact != null) {
            parameters.getRecord().set(parameters.getSettings().getAddressPosition(), cachedContact.getAddress());
            parameters.getRecord().set(parameters.getSettings().getPhonePosition(), cachedContact.getPhoneNumber());
        } else {
            PhoneQuery phoneQuery = new PhoneQuery();
            String formattedPhone = CommonUtils.formatPhoneNumber(parameters.getContact().getPhoneNumber());
            phoneQuery.setPhone(formattedPhone);

            try {
                List<Phone> phoneList = lookupService.findByPhone(parameters.getClient(), phoneQuery);

                if (phoneList != null && phoneList.size() > 0) {
                    List<LegalEntity> legalEntities = new ArrayList<>();
                    for (Phone phone : phoneList) {
                        if (phone.getPeople() != null) {
                            legalEntities.addAll(phone.getPeople());
                        }
                        if (phone.getBusinesses() != null) {
                            legalEntities.addAll(phone.getBusinesses());
                        }
                    }
                    if (legalEntities != null && legalEntities.size() > 0) {
                        List<LegalEntity> bestMatchEntities = legalEntities.stream()
                                .filter(le -> le.getLocations() != null && le.getLocations().size() > 0).collect(Collectors.toList());

                        if (bestMatchEntities != null && bestMatchEntities.size() > 0) {
                            if (StringUtils.isBlank(parameters.getContact().getAddress())) {
                                LegalEntity bestMatchEntity = bestMatchEntities.get(0);
                                Location location = getLocation(bestMatchEntity);
                                if (location != null) {
                                    parameters.getRecord().set(parameters.getSettings().getAddressPosition(), location.getAddress());
                                }
                                parameters.getRecord().set(parameters.getSettings().getNamePosition(), bestMatchEntity.getName());
                            } else {
                                final Address copy = new Address(parameters.getAddress());
                                LegalEntity bestMatchEntity = bestMatchEntities.stream()
                                        .filter(le -> lookupService.isLocationMatch(le.getLocations().get(0), copy)).findFirst().orElse(bestMatchEntities.get(0));

                                parameters.getRecord().set(parameters.getSettings().getNamePosition(), bestMatchEntity.getName());
                            }
                        } else {
                            LegalEntity bestMatchEntity = legalEntities.get(0);
                            parameters.getRecord().set(parameters.getSettings().getNamePosition(), bestMatchEntity.getName());
                        }
                    }
                }
            } catch (FindException fe) {
                LOGGER.error("ReversePhone lookup failed!", fe);
            }
            updateCaches(parameters.getRecord().get(parameters.getSettings().getNamePosition()),
                    parameters.getContact().getPhoneNumber(),
                    parameters.getRecord().get(parameters.getSettings().getAddressPosition()));
        }
    }

    private void locationLookup(LookupParameters parameters) {
        Contact cachedContact = checkCaches(parameters.getContact());
        if (cachedContact != null) {
            parameters.getRecord().set(parameters.getSettings().getAddressPosition(), cachedContact.getAddress());
            parameters.getRecord().set(parameters.getSettings().getPhonePosition(), cachedContact.getPhoneNumber());
        } else {
            LocationQuery locationQuery = new LocationQuery();
            locationQuery.setStreetLine1(parameters.getAddress().getStreet());
            locationQuery.setCity(parameters.getAddress().getCity());
            locationQuery.setPostalCode(parameters.getAddress().getZip());
            locationQuery.setStateCode(parameters.getAddress().getState());

            try {
                List<Location> locationList = lookupService.findByLocation(parameters.getClient(), locationQuery);

                if (locationList != null && locationList.size() > 0) {
                    List<LegalEntity> legalEntities = new ArrayList<>();
                    for (Location loc : locationList) {
                        if (loc.getPeople() != null) {
                            legalEntities.addAll(loc.getPeople());
                        }
                        if (loc.getBusinesses() != null) {
                            legalEntities.addAll(loc.getBusinesses());
                        }
                    }

                    LegalEntity legalEntity = legalEntities.stream().filter(p -> p.getPhones() != null && p.getPhones().size() > 0).findFirst().orElse(null);
                    if (legalEntity != null) {
                        parameters.getRecord().set(parameters.getSettings().getNamePosition(), legalEntity.getName());
                        parameters.getRecord().set(parameters.getSettings().getPhonePosition(), legalEntity.getPhones().get(0).getPhoneNumber());
                    }
                }
            } catch (FindException fe) {
                LOGGER.error("ReverseAddress lookup failed!", fe);
            }
        }
    }

    private void updateCaches(String name, String phoneNumber, String address) {
        Contact contact = new Contact(name, phoneNumber, address);

        if (contact.getName() != null) {
            personCache.put(contact.getName(), contact);
        }
        if (contact.getPhoneNumber() != null) {
            phoneCache.put(contact.getPhoneNumber(), contact);
        }
        if (contact.getAddress() != null) {
            locationCache.put(contact.getAddress(), contact);
        }

        LOGGER.debug("Added to cache: {}", contact);
    }

    private Contact checkCaches(Contact contact) {
        Contact cachedContact = personCache.getIfPresent(contact.getName());
        if (cachedContact == null) {
            cachedContact = phoneCache.getIfPresent(contact.getPhoneNumber());
        }
        if (cachedContact == null) {
            cachedContact = locationCache.getIfPresent(contact.getAddress());
        }

        if (cachedContact != null) {
            LOGGER.debug("Cache hit: {}", cachedContact);
        }

        return cachedContact;
    }

    private Location getLocation(LegalEntity legalEntity) {
        Location location = null;
        if (legalEntity.getLocations() != null && legalEntity.getLocations().size() > 0) {
            location = legalEntity.getLocations().get(0);
        }
        return location;
    }

    private static class LookupParameters {
        private final Address address;
        private final JobSettings settings;
        private final Client client;
        private final CSVRecord record;
        private final Contact contact;

        public LookupParameters(Address address, JobSettings settings, Client client, CSVRecord record, Contact contact) {
            this.address = address;
            this.settings = settings;
            this.client = client;
            this.record = record;
            this.contact = contact;
        }

        public Address getAddress() {
            return address;
        }

        public JobSettings getSettings() {
            return settings;
        }

        public Client getClient() {
            return client;
        }

        public CSVRecord getRecord() {
            return record;
        }

        public Contact getContact() {
            return contact;
        }
    }

}

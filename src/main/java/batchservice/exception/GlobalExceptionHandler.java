package batchservice.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

//@ControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BatchException.class)
    public @ResponseBody ErrorInfo handleBatchException(BatchException ex){
        LOGGER.error("An error occurred during processing the batch request: ", ex);

        return new ErrorInfo(ex.getClass().getName(), ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public @ResponseBody ErrorInfo defaultErrorHandler(Exception ex){
        LOGGER.error("An unexpected error occurred: ", ex);

        return new ErrorInfo(ex.getClass().getName(), ex.getMessage());
    }


    private static class ErrorInfo {
        private String source;
        private String message;

        public ErrorInfo(String source, String message) {
            this.source = source;
            this.message = message;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}

package batchservice.exception;

public class BatchException extends RuntimeException {
    public BatchException(String message) {
        super(message);
    }

    public BatchException(String message, Throwable throwable) {
        super(message, throwable);
    }
}


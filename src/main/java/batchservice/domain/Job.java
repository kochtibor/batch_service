package batchservice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Job implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", unique = true, nullable = false)
    private long id;

    @Column(name = "DIR", length = 80, nullable = false)
    private String jobDirectory;

    @Column(name = "STATUS", nullable = false)
    private JobStatus status = JobStatus.QUEUED;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> messages = new ArrayList<>();

    @Column(name = "CREATION_DATE", nullable = false)
    private Date creationDate = new Date();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getJobDirectory() {
        return jobDirectory;
    }

    public void setJobDirectory(String jobDirectory) {
        this.jobDirectory = jobDirectory;
    }

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Job job = (Job) o;

        if (id != job.id) return false;
        if (creationDate != null ? !creationDate.equals(job.creationDate) : job.creationDate != null) return false;
        if (jobDirectory != null ? !jobDirectory.equals(job.jobDirectory) : job.jobDirectory != null) return false;
        if (messages != null ? !messages.equals(job.messages) : job.messages != null) return false;
        if (status != job.status) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (jobDirectory != null ? jobDirectory.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (messages != null ? messages.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Job{" +
                "id=" + id +
                ", jobDirectory='" + jobDirectory + '\'' +
                ", status=" + status +
                ", messages=" + messages +
                ", creationDate=" + creationDate +
                '}';
    }
}

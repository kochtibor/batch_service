package batchservice.domain;

import java.util.ArrayList;
import java.util.List;

public class StatusResponse {
    private JobStatus status;
    private String resource;
    private List<String> messages = new ArrayList<>();

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}

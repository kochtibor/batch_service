package batchservice.domain;

public enum JobStatus {
    QUEUED,
    IN_PROGRESS,
    FINISHED,
    FAILED
}

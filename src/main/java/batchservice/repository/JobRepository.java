package batchservice.repository;

import batchservice.domain.Job;
import batchservice.domain.JobStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface JobRepository extends CrudRepository<Job, Long> {

    @Query("select j from Job j where j.creationDate < ?1 and status = ?2")
    List<Job> findByCutoffDateAndStatus(Date cutoffDate, JobStatus status);
}
package batchservice.utils;

import batchservice.service.JobSettings;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public final class CommonUtils {

    private static final String NAME = "name";
    private static final String ADDRESS = "address";
    private static final String FULL_ADDRESS = "fulladdress";
    private static final String PHONE = "phone";
    private static final String PHONE_NUMBER = "phonenumber";
    private static final String NUMBER = "number";
    private static final String CITY_STATE_ZIP = "citystatezip";
    private static final String CITY = "city";
    private static final String STATE = "state";
    private static final String ZIP = "zip";

    public static JobSettings getColumnPositions(Map<String, Integer> headerMap) {
        JobSettings settings = new JobSettings();
        for (Map.Entry<String, Integer> entry : headerMap.entrySet()) {
            String lowerCaseHeader = entry.getKey().toLowerCase().replaceAll("[^A-Za-z ]", "");

            if (StringUtils.getLevenshteinDistance(lowerCaseHeader, PHONE) < 2 ||
                    StringUtils.getLevenshteinDistance(lowerCaseHeader, PHONE_NUMBER) < 2 ||
                    (lowerCaseHeader.contains(PHONE) && lowerCaseHeader.contains(NUMBER))) {
                settings.setPhonePosition(entry.getValue());
            }

            if (StringUtils.getLevenshteinDistance(lowerCaseHeader, ADDRESS) < 2 ||
                    StringUtils.getLevenshteinDistance(lowerCaseHeader, FULL_ADDRESS) < 2 ||
                    StringUtils.getLevenshteinDistance(lowerCaseHeader, CITY_STATE_ZIP) < 2 ||
                    (lowerCaseHeader.contains(CITY) && lowerCaseHeader.contains(STATE) && lowerCaseHeader.contains(ZIP))) {
                settings.setAddressPosition(entry.getValue());
            }

            if (StringUtils.getLevenshteinDistance(lowerCaseHeader, NAME) < 2 ||
                    lowerCaseHeader.contains(NAME)) {
                settings.setNamePosition(entry.getValue());
            }
        }

        return settings;
    }

    public static String formatPhoneNumber(String phone) {
        return phone.replaceAll("[^\\d]", "");
    }

}

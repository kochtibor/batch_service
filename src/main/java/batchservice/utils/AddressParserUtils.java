package batchservice.utils;

import batchservice.domain.Address;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class AddressParserUtils {

    private static final Pattern ZIP_CODE_PATTERN = Pattern.compile("^\\d{5}(?:[-\\s]\\d{4})?$");
    private static final Pattern STATE_CODE_PATTERN = Pattern.compile("^[A-Z]{2}$");

    public static Address parseAddress(String addressString) {
        Address address = new Address();

        List<String> parts = Lists.newArrayList(Splitter.on(",").split(addressString));
        if (parts.size() == 1) {
            parseCityStateZip(parts.get(0), address);
        } else if (parts.size() > 1) {
            String lastPart = parts.get(parts.size() - 1);
            parseCityStateZip(lastPart, address);

            List<String> streetElements = parts.subList(0, parts.size() - 1);
            streetElements.removeIf(elem -> StringUtils.isBlank(elem));
            streetElements.replaceAll(elem -> elem.trim().replaceAll(" +", " "));
            address.setStreet(Joiner.on(" ").skipNulls().join(streetElements));
        }

        return address;
    }

    private static Address parseCityStateZip(String cityStateZip, Address address) {
        List<String> parts = Lists.newArrayList(Splitter.on(" ").split(cityStateZip));
        String zip = parseZip(parts.get(parts.size() - 1));
        if (zip != null) {
            address.setZip(zip);
            parts.remove(parts.size() - 1);
        }

        if (parts.size() > 0) {
            String state = parseState(parts.get(parts.size() - 1));
            if (state != null) {
                address.setState(state);
                parts.remove(parts.size() - 1);
            }
        }

        if (parts.size() > 0) {
            parts.removeIf(elem -> StringUtils.isBlank(elem));
            address.setCity(Joiner.on(" ").skipNulls().join(parts));
        }

        return address;
    }

    private static String parseZip(String zipCandidate) {
        String result = null;
        Matcher matcher = ZIP_CODE_PATTERN.matcher(zipCandidate);
        if (matcher.matches()) {
            result = zipCandidate;
        }
        return result;
    }

    private static String parseState(String stateCandidate) {
        String result = null;
        Matcher matcher = STATE_CODE_PATTERN.matcher(stateCandidate);
        if (matcher.matches()) {
            result = stateCandidate;
        }
        return result;
    }
}

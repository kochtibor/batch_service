package batchservice;

import batchservice.domain.Address;
import batchservice.service.LookupService;
import com.whitepages.proapi.data.entity.Location;
import com.whitepages.proapi.data.entity.LocationImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class LocationMatchTest {

    LookupService lookupService;

    @Before
    public void setUp() {
        lookupService = new LookupService();
    }

    @Test
    public void testNoMatch() {
        Address address = new Address();
        address.setCity("Ashland");
        address.setState("TN");
        address.setZip("59004");

        LocationImpl location = new LocationImpl();
        location.setCity("Ashland");
        location.setStateCode("MT");
        location.setPostalCode("59004");

        boolean isMatch = lookupService.isLocationMatch(location, address);

        assertFalse(isMatch);
    }

    @Test
    public void testMatchWithoutStreet() {
        Address address = new Address();
        address.setCity("Ashland");
        address.setState("MT");
        address.setZip("59004");

        LocationImpl location = new LocationImpl();
        location.setCity("Ashland");
        location.setStateCode("MT");
        location.setPostalCode("59004");

        boolean isMatch = lookupService.isLocationMatch(location, address);

        assertTrue(isMatch);
    }

    @Test
    public void testMatchWithIncorrectStreet() {
        Address address = new Address();
        address.setCity("Ashland");
        address.setState("MT");
        address.setZip("59004");
        address.setStreet("271 Grenadier Ave SW");

        LocationImpl location = new LocationImpl();
        location.setCity("Ashland");
        location.setStateCode("MT");
        location.setPostalCode("59004");
        location.setAddress("270 Grenadier Ave SW, Ashland MT 59004");

        boolean isMatch = lookupService.isLocationMatch(location, address);

        assertFalse(isMatch);
    }

    @Test
    public void testMatchWithStateCode() {
        Address address = new Address();
        address.setState("AK");

        LocationImpl location = new LocationImpl();
        location.setCity("Ashland");
        location.setStateCode("AK");
        location.setPostalCode("59003");

        boolean isMatch = lookupService.isLocationMatch(location, address);

        assertTrue(isMatch);
    }

    @Test
    public void testMatchWithFullAddress() {
        Address address = new Address();
        address.setCity("Ashland");
        address.setState("MT");
        address.setZip("59004");
        address.setStreet("270 Grenadier Ave SW");

        LocationImpl location = new LocationImpl();
        location.setCity("Ashland");
        location.setStateCode("MT");
        location.setPostalCode("59004");
        location.setAddress("Building A 2nd floor, 270 Grenadier Ave SW, Ashland MT 59004");

        boolean isMatch = lookupService.isLocationMatch(location, address);

        assertTrue(isMatch);
    }
}

package batchservice;

import batchservice.configuration.AppConfiguration;
import batchservice.csv.CSVRecord;
import batchservice.domain.Contact;
import batchservice.repository.JobRepository;
import batchservice.service.BatchService;
import batchservice.service.JobSettings;
import batchservice.service.LookupService;
import com.google.common.cache.Cache;
import com.whitepages.proapi.api.client.FindException;
import com.whitepages.proapi.data.entity.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.spy;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfiguration.class, RecordUpdateTest.TestConfiguration.class})
public class RecordUpdateTest {

    JobSettings settings = new JobSettings();

    @Autowired
    BatchService batchService;

    @Autowired
    LookupService lookupService;

    @Configuration
    static class TestConfiguration {
        @Bean
        public BatchService batchService() {
            BatchService batchService = new BatchService();
            return batchService;
        }

        @Bean
        public LookupService lookupService() {
            LookupService lookupService = mock(LookupService.class);
            return lookupService;
        }

        @Bean(name = "personCache")
        public Cache<String, Contact> personCache() {
            Cache<String, Contact> personCache = mock(Cache.class);
            return personCache;
        }

        @Bean(name = "locationCache")
        public Cache<String, Contact> locationCache() {
            Cache<String, Contact> locationCache = mock(Cache.class);
            return locationCache;
        }

        @Bean(name = "phoneCache")
        public Cache<String, Contact> phoneCache() {
            Cache<String, Contact> phoneCache = mock(Cache.class);
            return phoneCache;
        }
    }

    @Before
    public void setUp() {
        this.settings.setNamePosition(0);
        this.settings.setPhonePosition(1);
        this.settings.setAddressPosition(2);
    }

    @Test
    public void testNameLookup() throws FindException {
        List<CSVRecord> recordList = new ArrayList<>();
        CSVRecord record = new CSVRecord(new String[] {"Matthew K Woodward", "", ""}, null, null, 1, 0);
        recordList.add(record);

        List<Person> people = new ArrayList<>();

        PersonImpl person = mock(PersonImpl.class);
        person.setBestName("Test User");
        List<Location> locationList = new ArrayList<>();
        LocationImpl location = new LocationImpl();
        location.setAddress("Ashland MT 59004");
        locationList.add(location);
        when(person.getLocations()).thenReturn(locationList);
        PhoneImpl phone = new PhoneImpl();
        phone.setPhoneNumber("646-480-6649");
        List<Phone> phoneList = new ArrayList<>();
        phoneList.add(phone);
        when(person.getPhones()).thenReturn(phoneList);
        people.add(person);

        when(lookupService.findByPerson(anyObject(), anyObject())).thenReturn(people);

        batchService.updateData(this.settings, recordList);

        assertEquals(record.get(1), "646-480-6649");
        assertEquals(record.get(2), "Ashland MT 59004");
    }

    @Test
    public void testNameLookupWithBusinesses() throws FindException {
        List<CSVRecord> recordList = new ArrayList<>();
        CSVRecord record = new CSVRecord(new String[] {"Whitepages", "", ""}, null, null, 1, 0);
        recordList.add(record);

        List<Business> businesses = new ArrayList<>();

        BusinessImpl business = mock(BusinessImpl.class);
        business.setName("Whitepages");
        List<Location> locationList = new ArrayList<>();
        LocationImpl location = new LocationImpl();
        location.setAddress("920 Broadway, New York, NY 10010-6004");
        locationList.add(location);
        when(business.getLocations()).thenReturn(locationList);
        PhoneImpl phone = new PhoneImpl();
        phone.setPhoneNumber("206-973-5100");
        List<Phone> phoneList = new ArrayList<>();
        phoneList.add(phone);
        when(business.getPhones()).thenReturn(phoneList);
        businesses.add(business);

        when(lookupService.findByPerson(anyObject(), anyObject())).thenReturn(null);
        when(lookupService.findByBusiness(anyObject(), anyObject())).thenReturn(businesses);

        batchService.updateData(this.settings, recordList);

        assertEquals(record.get(1), "206-973-5100");
        assertEquals(record.get(2), "920 Broadway, New York, NY 10010-6004");
    }

    @Test
    public void testPhoneLookupWithoutLocations() throws FindException {
        List<CSVRecord> recordList = new ArrayList<>();
        CSVRecord record = new CSVRecord(new String[]{"", "646-480-6649", "Ashland MT 59004"}, null, null, 1, 0);
        recordList.add(record);

        PhoneImpl phone1 = mock(PhoneImpl.class);
        phone1.setPhoneNumber("646-480-6649");
        PersonImpl person1 = new PersonImpl();
        person1.setBestName("Test User1");
        List<Person> people1 = new ArrayList<>();
        people1.add(person1);
        when(phone1.getPeople()).thenReturn(people1);

        PhoneImpl phone2 = mock(PhoneImpl.class);
        phone2.setPhoneNumber("646-480-6649");
        PersonImpl person2 = new PersonImpl();
        person2.setBestName("Test User2");
        List<Person> people2 = new ArrayList<>();
        people2.add(person2);
        when(phone2.getPeople()).thenReturn(people2);

        List<Phone> phoneList = new ArrayList<>();
        phoneList.add(phone1);
        phoneList.add(phone2);

        when(lookupService.findByPhone(anyObject(), anyObject())).thenReturn(phoneList);

        batchService.updateData(this.settings, recordList);

        assertEquals(record.get(0), "Test User1");
    }

    @Test
    public void testPhoneLookupWithLocations() throws FindException {
        List<CSVRecord> recordList = new ArrayList<>();
        CSVRecord record = new CSVRecord(new String[]{"", "646-480-6649", "Seattle WA"}, null, null, 1, 0);
        recordList.add(record);

        PhoneImpl phone1 = mock(PhoneImpl.class);
        phone1.setPhoneNumber("646-480-6649");
        PersonImpl person1 = new PersonImpl();
        PersonImpl spy1 = spy(person1);
        spy1.setBestName("Test User1");
        List<Location> locationList1 = new ArrayList<>();
        LocationImpl location1 = new LocationImpl();
        location1.setAddress("White Plains NY 10605");
        locationList1.add(location1);
        when(spy1.getLocations()).thenReturn(locationList1);
        List<Person> people1 = new ArrayList<>();
        people1.add(spy1);
        when(phone1.getPeople()).thenReturn(people1);

        PhoneImpl phone2 = mock(PhoneImpl.class);
        phone2.setPhoneNumber("646-480-6649");
        PersonImpl person2 = new PersonImpl();
        PersonImpl spy2 = spy(person2);
        spy2.setBestName("Test User2");
        List<Location> locationList2 = new ArrayList<>();
        LocationImpl location2 = new LocationImpl();
        location2.setAddress("Seattle WA 98119-2043");
        locationList2.add(location2);
        when(spy2.getLocations()).thenReturn(locationList2);
        List<Person> people2 = new ArrayList<>();
        people2.add(spy2);
        when(phone2.getPeople()).thenReturn(people2);

        List<Phone> phoneList = new ArrayList<>();
        phoneList.add(phone1);
        phoneList.add(phone2);

        when(lookupService.findByPhone(anyObject(), anyObject())).thenReturn(phoneList);
        when(lookupService.isLocationMatch(eq(location2), anyObject())).thenReturn(true);

        batchService.updateData(this.settings, recordList);

        assertEquals(record.get(0), "Test User2");

        recordList.clear();
        record = new CSVRecord(new String[]{"", "646-480-6649", "Ashland MT 59004-0001"}, null, null, 1, 0);
        recordList.add(record);

        when(lookupService.isLocationMatch(anyObject(), anyObject())).thenReturn(false);

        batchService.updateData(this.settings, recordList);

        assertEquals(record.get(0), "Test User1");

        // with empty address
        recordList.clear();
        record = new CSVRecord(new String[]{"", "646-480-6649", ""}, null, null, 1, 0);
        recordList.add(record);

        batchService.updateData(this.settings, recordList);

        assertEquals(record.get(0), "Test User1");
        assertEquals(record.get(2), "White Plains NY 10605");
    }

    @Test
    public void testLocationLookup() throws FindException {
        List<CSVRecord> recordList = new ArrayList<>();
        CSVRecord record = new CSVRecord(new String[] {"", "", "3023 Hansberry Ave, Ashland MT 59004-0001"}, null, null, 1, 0);
        recordList.add(record);

        LocationImpl location1 = mock(LocationImpl.class);
        PersonImpl person1 = new PersonImpl();
        List<Person> people1 = new ArrayList<>();
        people1.add(person1);
        when(location1.getPeople()).thenReturn(people1);

        LocationImpl location2 = mock(LocationImpl.class);
        PersonImpl person2 = new PersonImpl();
        List<Person> people2 = new ArrayList<>();
        people2.add(person2);
        when(location2.getPeople()).thenReturn(people2);

        List<Location> locationList = new ArrayList<>();
        locationList.add(location1);
        locationList.add(location2);

        when(lookupService.findByLocation(anyObject(), anyObject())).thenReturn(locationList);

        batchService.updateData(this.settings, recordList);

        assertEquals(record.get(0), "");
        assertEquals(record.get(1), "");

        PersonImpl spy = spy(person2);
        spy.setBestName("Test User1");
        PhoneImpl phone = new PhoneImpl();
        phone.setPhoneNumber("6464806649");
        List<Phone> phoneList = new ArrayList<>();
        phoneList.add(phone);
        when(spy.getPhones()).thenReturn(phoneList);
        people2.clear();
        people2.add(spy);

        batchService.updateData(this.settings, recordList);

        assertEquals(record.get(0), "Test User1");
        assertEquals(record.get(1), "6464806649");
    }
}

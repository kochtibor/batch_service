package batchservice;

import batchservice.domain.Address;
import batchservice.utils.AddressParserUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AddressParserTest {

    @Test
    public void testAddressWithoutZip() {
        String addressString = "Mapple  Valley WA";
        Address address = AddressParserUtils.parseAddress(addressString);

        assertEquals(address.getCity(), "Mapple Valley");
        assertEquals(address.getState(), "WA");
        assertEquals(address.getZip(), null);
    }

    @Test
    public void testAddressWithWhitespaces() {
        String addressString = "  Mapple  Valley   WA";
        Address address = AddressParserUtils.parseAddress(addressString);

        assertEquals(address.getCity(), "Mapple Valley");
        assertEquals(address.getState(), "WA");
        assertEquals(address.getZip(), null);
    }

    @Test
    public void testAddressWithCityStateZip() {
        String addressString = "Mapple  Valley  WA 98038";
        Address address = AddressParserUtils.parseAddress(addressString);

        assertEquals(address.getCity(), "Mapple Valley");
        assertEquals(address.getState(), "WA");
        assertEquals(address.getZip(), "98038");
    }

    @Test
    public void testAddressWithStreetName() {
        String addressString = " 302 Gorham  Ave, Mapple  Valley  WA 98038";
        Address address = AddressParserUtils.parseAddress(addressString);

        assertEquals(address.getCity(), "Mapple Valley");
        assertEquals(address.getState(), "WA");
        assertEquals(address.getZip(), "98038");
        assertEquals(address.getStreet(), "302 Gorham Ave");
    }

    @Test
    public void testAddressWithMultipleParts() {
        String addressString = "  2nd floor,  302 Gorham  Ave, Mapple  Valley  WA 98038";
        Address address = AddressParserUtils.parseAddress(addressString);

        assertEquals(address.getCity(), "Mapple Valley");
        assertEquals(address.getState(), "WA");
        assertEquals(address.getZip(), "98038");
        assertEquals(address.getStreet(), "2nd floor 302 Gorham Ave");
    }
}

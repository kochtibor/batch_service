package batchservice;

import batchservice.service.JobSettings;
import batchservice.utils.CommonUtils;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

public class HeaderParsingTest {

    @Test
    public void testNormalMatch() {
        Map<String, Integer> headerMap = new LinkedHashMap<>();
        headerMap.put("Name", 0);
        headerMap.put("Address", 1);
        headerMap.put("Phone", 2);

        JobSettings settings = CommonUtils.getColumnPositions(headerMap);
        assertTrue(settings.getNamePosition() > -1);
        assertTrue(settings.getAddressPosition() > -1);
        assertTrue(settings.getPhonePosition() > -1);
    }

    @Test
    public void testTypoMatch() {
        Map<String, Integer> headerMap = new LinkedHashMap<>();
        headerMap.put("Namr", 0);
        headerMap.put("Address", 1);
        headerMap.put("Phone#", 2);

        JobSettings settings = CommonUtils.getColumnPositions(headerMap);
        assertTrue(settings.getNamePosition() > -1);
        assertTrue(settings.getAddressPosition() > -1);
        assertTrue(settings.getPhonePosition() > -1);
    }

    @Test
    public void testFieldLikeNamesMatch() {
        Map<String, Integer> headerMap = new LinkedHashMap<>();
        headerMap.put("name", 0);
        headerMap.put("full_address", 1);
        headerMap.put("phone_number", 2);

        JobSettings settings = CommonUtils.getColumnPositions(headerMap);
        assertTrue(settings.getNamePosition() > -1);
        assertTrue(settings.getAddressPosition() > -1);
        assertTrue(settings.getPhonePosition() > -1);
    }

    @Test
    public void testWithWhitespacesMatch() {
        Map<String, Integer> headerMap = new LinkedHashMap<>();
        headerMap.put("name  ", 0);
        headerMap.put(" city,  state zip", 1);
        headerMap.put("  phone  number", 2);

        JobSettings settings = CommonUtils.getColumnPositions(headerMap);
        assertTrue(settings.getNamePosition() > -1);
        assertTrue(settings.getAddressPosition() > -1);
        assertTrue(settings.getPhonePosition() > -1);
    }

    @Test
    public void testNoMatch() {
        Map<String, Integer> headerMap = new LinkedHashMap<>();
        headerMap.put("Name", 0);
        headerMap.put("This is Sparta!", 1);
        headerMap.put("Phone number", 2);

        JobSettings settings = CommonUtils.getColumnPositions(headerMap);
        assertTrue(settings.getNamePosition() > -1);
        assertTrue(settings.getAddressPosition() == -1);
        assertTrue(settings.getPhonePosition() > -1);
    }
}
